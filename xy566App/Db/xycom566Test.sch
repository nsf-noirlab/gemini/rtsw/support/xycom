[schematic2]
uniq 2
[tools]
[detail]
w 362 850 -100 0 n#1 hwin.hwin#73.in 352 840 352 840 eais.aiS14.INP
[cell use]
use eais 470 731 100 0 aiS14
xform 0 480 808
p 386 731 100 0 -1 PV:xy566:
p 354 889 100 0 1 DTYP:XYCOM-566 Dif Scanned
p 303 700 100 0 1 EGUF:10.0
p 303 673 100 0 1 EGUL:-10.0
p 303 773 100 0 1 MDEL:-1
p 412 867 100 0 1 SCAN:.1 second
use hwin 160 824 100 0 hwin#73
xform 0 256 840
p 163 832 100 0 -1 val(in):#C0 S14
[comments]
